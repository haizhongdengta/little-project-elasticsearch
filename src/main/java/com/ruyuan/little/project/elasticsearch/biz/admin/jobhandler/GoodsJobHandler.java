package com.ruyuan.little.project.elasticsearch.biz.admin.jobhandler;

import com.ruyuan.little.project.elasticsearch.biz.admin.service.AdminGoodsService;
import com.ruyuan.little.project.elasticsearch.biz.common.constant.StringPoolConstant;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


/**
 * XxlJob开发示例（Bean模式）
 * <p>
 * 开发步骤：
 * 1、在Spring Bean实例中，开发Job方法，方式格式要求为 "public ReturnT<String> execute(String param)"
 * 2、为Job方法添加注解 "@XxlJob(value="自定义jobhandler名称", init = "JobHandler初始化方法", destroy = "JobHandler销毁方法")"，注解value值对应的是调度中心新建任务的JobHandler属性的值。
 * 3、执行日志：需要通过 "XxlJobLogger.log" 打印执行日志；
 *
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:商品job handler
 */
@Component
public class GoodsJobHandler {

    /**
     * 商品service组件
     */
    @Autowired
    private AdminGoodsService adminGoodsService;

    /**
     * 自动上架商品job
     *
     * @return 结果
     * @throws Exception
     */
    @XxlJob(value = "autoOnlineGoodsJobHandler")
    public ReturnT<String> autoOnlineGoodsJobHandler(String param) throws Exception {
        XxlJobLogger.log("开始执行商品自动上架定时任务");

        // 不采用分页的方式自动上架商品
//        adminGoodsService.autoOnlineGoodsSpuByOnlineTime(
//                LocalDateTime.now().format(DateTimeFormatter.ofPattern(StringPoolConstant.FULL_DATE_TIME)));
//
        // 采用scroll分页的方式自动上架商品
        adminGoodsService.autoOnlineGoodsSpuByOnlineTimeScroll(
                        LocalDateTime.now().format(DateTimeFormatter.ofPattern(StringPoolConstant.FULL_DATE_TIME)));

        XxlJobLogger.log("执行商品自动上架定时任务完成");
        return ReturnT.SUCCESS;
    }
}

