package com.ruyuan.little.project.elasticsearch.biz.api.controller;

import com.ruyuan.little.project.common.dto.CommonResponse;
import com.ruyuan.little.project.elasticsearch.biz.api.dto.OrderSearchDTO;
import com.ruyuan.little.project.elasticsearch.biz.api.entity.Order;
import com.ruyuan.little.project.elasticsearch.biz.api.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:elasticsearch实战
 **/

@RestController
@RequestMapping("/api/order")
public class OrderController {

    /**
     * 订单服务组件
     */
    @Autowired
    private OrderService orderService;

    /**
     * 创建订单
     *
     * @param order 订单信息
     * @return 结果
     */
    @PostMapping("/create")
    public CommonResponse createOrder(Order order) {
        return orderService.createOrder(order);
    }

    /**
     * 支付订单
     *
     * @param order 订单信息
     * @return 结果
     */
    @PostMapping("/pay")
    public CommonResponse pay(Order order) {
        return orderService.pay(order);
    }

    /**
     * 取消订单
     *
     * @param order 订单信息
     * @return 结果
     */
    @PostMapping("/cancel")
    public CommonResponse cancel(Order order) {
        return orderService.cancel(order);
    }

    /**
     * 确认收货
     *
     * @param order 订单信息
     * @return 结果
     */
    @PostMapping("/receive")
    public CommonResponse receive(Order order) {
        return orderService.receive(order);
    }

    /**
     * 订单评价
     *
     * @param order 订单信息
     * @return 结果
     */
    @PostMapping("/comment")
    public CommonResponse publishComment(Order order) {
        return orderService.publishComment(order);
    }

    /**
     * 根据id获取订单详情
     *
     * @param id 订单id
     * @return 结果
     */
    @GetMapping("/{id}")
    public CommonResponse getOrderById(@PathVariable String id) {
        return orderService.getOrderById(id);
    }


    /**
     * 获取订单列表
     *
     * @param orderSearchDTO 订单查询请求信息
     * @return               结果
     */
    @PostMapping("/list")
    public CommonResponse getOrderList(OrderSearchDTO orderSearchDTO) {
        return orderService.getOrderList(orderSearchDTO);
    }

}
